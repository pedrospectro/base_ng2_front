import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import { AboutComponent }  from './components/about/about.component';
import { HomeComponent }  from './components/home/home.component';
import { ProfileComponent }  from './components/profile/profile.component';

const appRoutes: Routes = [
    {
        path:"",
        component:HomeComponent
    },
    {
        path:"about",
        component:AboutComponent
    },
   	{
        path:"profile",
        component:ProfileComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);