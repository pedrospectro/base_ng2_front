import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService{

    private url:string;
    private version:string;
    private endpoint:string;
    private signupUrl:string;
    private loginUrl:string;
    private api_key:string;
    

    constructor(private _http:Http) {
        this.url="http://localhost:4000/";
        this.version="api/v1/";
        this.endpoint=this.url+this.version;
    }

    public signup(data:any){
        this.signupUrl = this.endpoint+'signup';
        return this._http.post(this.signupUrl,data).map(res => res.json());
    }

    public login(data:any){
        this.loginUrl = this.endpoint+'login';
        return this._http.post(this.loginUrl,data).map(res => res.json());
    }

    public authenticated()
    {
        this.api_key=localStorage.getItem("api_key");
        if(this.api_key)
            return true;
        else
            return false;
    }
}