import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';


@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: 'home.component.html',
})
export class HomeComponent  {
	data:any;
	email:string;
	password:string;
	authenticated:any;
	signinData:any

	constructor(private _apiService:ApiService){
		this.authenticated=_apiService.authenticated();
	}

	setLocalStorage(user:any)
	{
		localStorage.setItem('api_key',user.api_key);
		localStorage.setItem('profile',JSON.stringify(user));	
		this.authenticated=this._apiService.authenticated();	
	}

	signup(){
		this.data = {
			"email":this.email,
			"password":this.password
		}
		this._apiService.signup(this.data)
            .subscribe(res => {
                this.signinData = res;
                this.setLocalStorage(this.signinData);
                window.location.reload();
        });
	}
}