import { Component } from '@angular/core';
import {ApiService} from '../../services/api.service';

@Component({
  moduleId: module.id,
  selector: 'profile',
  templateUrl: 'profile.component.html'
})
export class ProfileComponent  {  
  profile: any;

  constructor(private _apiService:ApiService){
    this.profile=JSON.parse(localStorage.getItem('profile'));
  }
}
