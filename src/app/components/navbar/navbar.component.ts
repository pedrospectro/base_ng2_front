import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
})
export class NavbarComponent{
  profile:any;

  constructor(){
  	this.profile=localStorage.getItem('profile')
  }

  logout()
  {
  	localStorage.clear();
  	window.location.reload();
  }
}