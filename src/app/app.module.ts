import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http'; 
import { AppComponent }  from './app.component';

import { AboutComponent }  from './components/about/about.component';
import { NavbarComponent }  from './components/navbar/navbar.component';
import { HomeComponent }  from './components/home/home.component';
import { ProfileComponent }  from './components/profile/profile.component';

import {routing} from './app.routing';


@NgModule({
  imports:      [ BrowserModule, 
                  routing, 
                  FormsModule,
                  HttpModule 
                ],
  declarations: [ 
                  AppComponent,
                  AboutComponent,
                  HomeComponent,
                  ProfileComponent,
                  NavbarComponent
                ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
