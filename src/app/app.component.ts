import { Component } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  providers:[ApiService]
})
export class AppComponent  {}
